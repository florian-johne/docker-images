ARG QT_VERSION

FROM rabits/qt:${QT_VERSION}-android

ENV TZ 'Europe/Austria'
ENV SDK_PLATFORM android-29
ENV ANDROID_NDK_PLATFORM android-29
ENV SDK_BUILD_TOOLS=29.0.3
USER root

RUN echo $TZ > /etc/timezone && \
    apt-get update --fix-missing && \
    apt-get install -y --no-install-recommends git \
        wget \
        tzdata \
        g++ \
        build-essential \
        libc++-dev \
        python \
        openjdk-8-jdk \
        python \
        qbs && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    java -version && \
    qbs --version
    
#Create toolchain
RUN cd $ANDROID_NDK_ROOT/build/tools && \
    mkdir -p /opt/android-ndk/toolchains/llvm-4.9/prebuilt && \
    ./make-standalone-toolchain.sh --verbose --force --arch=arm --platform=$ANDROID_NDK_PLATFORM --install-dir=/opt/android-ndk/toolchains/llvm-4.9/prebuilt/linux-x86_64 --stl=libc++

#Download & unpack android SDK
RUN sudo rm -rf /opt/android-sdk && \
    sudo curl -Lo /tmp/sdk-tools.zip 'https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip' && \
    sudo mkdir -p /opt/android-sdk && \
    sudo unzip -q /tmp/sdk-tools.zip -d /opt/android-sdk && \
    sudo rm -rf /tmp/sdk-tools.zip && \
    sudo touch /root/.android/repositories.cfg && \
    sudo yes | sudo /opt/android-sdk/tools/bin/sdkmanager --licenses && \
    sudo /opt/android-sdk/tools/bin/sdkmanager "platforms;$SDK_PLATFORM" "build-tools;$SDK_BUILD_TOOLS" "tools" "platform-tools" | grep -v = || true


# configure qbs & Android sdk
RUN export QMAKE_INSTALL_PATH=$(which qmake | tail -n 1) && \
    export JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::") && \
    export PATH=$PATH:"/opt/android-ndk/toolchains/llvm-4.9/prebuilt/linux-x86_64/bin" && \
    export PATH=$PATH:$QT_ANDROID && \
    export SYSROOT="/opt/android-ndk/toolchains/llvm-4.9/prebuilt/linux-x86_64/sysroot" && \
    export CC=clang && \
    export CXX=clang++ && \
    qbs-setup-android --ndk-dir /opt/android-ndk --sdk-dir /opt/android-sdk --qt-dir $QT_ANDROID qt_android && \
    qbs-setup-toolchains --detect --system && \
    qbs config profiles.clang.qbs.sysroot $SYSROOT && \
    qbs config profiles.qt_android.baseProfile clang && \
    qbs config profiles.qt_android.java.jdkPath $JAVA_HOME && \
    qbs config profiles.qt_android.Android.ndk.platform $ANDROID_NDK_PLATFORM && \
    qbs config defaultProfile qt_android && \
    qbs config --list && \
    mkdir $HOME/.android
        
USER user

# cleanup
RUN sudo apt-get autoremove -y && \
    sudo apt-get -qq clean && \
    sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
